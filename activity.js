const http = require('http');

const port = 3000;

const server = http.createServer((req, res) =>{

	if (req.url == '/login') {
		res.writeHead(200, {'Content-type': 'text/plain'});
		res.end('Welcome to the login page.')
	}else{
		res.writeHead(404, {'Content-type': 'text/plain'});
		res.end("I'm sorry the page you are looking for cannot be found.")
	}

})

server.listen(port);
console.log(`Server now successfully running at port ${port}`);

//1. What directive is used by Node.js in loading modules it needs?

//Answer: require()

//2. What Node.js module contains a method for server creation?

//Answer: http module

//3. What is the method of the http object responsible for creating a server using Node.js?

//Answer: createServer() method

//4. What method of the response object allows us to set status codes and content types?

//Answer: writeHead() method

//5. Where will console.log() output its contents when run in Node.js?

//Answer: terminal/Git Bash

//6. What property of the request object contains the address' endpoint?

//Answer: .url()